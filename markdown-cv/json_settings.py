from pathutil import this_folder
import os.path

json_settings = os.path.join(this_folder(), 'markcv.json')

class JsonSettings():
    @staticmethod
    def read():    
        import json
        with open(json_settings, encoding='utf-8') as f:
             lines = [line for line in f.readlines() if not line.lstrip().startswith('//')] #strip comment lines
             return json.loads(''.join(lines))

    @staticmethod
    def allows():
        return ['.'.join([theme['name'], color.rpartition('-')[-1]]) 
                    for theme in JsonSettings.read()['themes'] 
                        for color in theme['colors']]
