import os
from os import path
from pathlib import Path
from datetime import date

import concurrent.futures

import logging
logger = logging.getLogger('.'.join(['cv', __name__]))

def shell_run(command):
    import subprocess
    logger.debug(command)
    cp = subprocess.run(command, shell=True,
                        stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logger.debug(
        f'args={cp.args} , returncode={cp.returncode} stdout={cp.stdout}, stderr={cp.stderr}')


g_themes = ['cerulean',
        #   'cosmo',
        #   'cyborg',
        #   'darkly',
        #   'flatly',
        #   'journal',
        #   'litera',
        #   'lumen',
        #   'lux',
        #   'materia',
        #   'minty',
        #   'pulse',
        #   'sandstone',
        #   'simplex',
        #   'sketchy',
        #   'slate',
        #   'solar',
        #   'spacelab',
        #   'superhero',
        #   'united',
          'yeti']
#
# bootswatch paths
#

VUE_CV_ROOT = Path(__file__).parent / 'cv'
RELEASE_ROOT = Path(__file__).parent / 'release'

# PDF_ROOT = VUE_CV_ROOT / 'dist/pdf'
PDF_ROOT = RELEASE_ROOT / 'pdf'
PDF_ROOT.mkdir(exist_ok=True)

DOCX_ROOT = RELEASE_ROOT / 'docx'
DOCX_ROOT.mkdir(exist_ok=True)



def generate_pdf(f):
    options = ' '.join([
        '--print-media-type',
        '--encoding utf-8',
        '--no-background',
        '--quiet',

        # '--image-quality 5',

        '--footer-font-size 6',
        '--footer-line',
        '--footer-left "[isodate]"',
        '--footer-center  "(+86) 135-248-26926  tom.tan@live.com"',
        '--footer-right " [page]/[toPage]"',

        '--header-font-size 6',
        '--header-right "[section]"',
        '--header-line'])

    def per_theme(theme):
        index = (RELEASE_ROOT/'index.html').as_uri()
        query_src = f'src={f.stem}'
        query_theme = f'theme={theme}'

        # e.g. "file:///F:/cv/temp/release/index.html?src=mini-cv-en&theme=yeti"
        url = f'"{index}?{query_theme}&{query_src}"'

        pdf = f.with_suffix(f'.{theme}.pdf').name

        return url, pdf

    # wkhtmltopdf %option% %src% %dest%
    for t in g_themes:
        url, pdf = per_theme(t)
        shell_run(f"wkhtmltopdf {options} {url} {PDF_ROOT / pdf}")
        logger.info(f'done with {url} -> {pdf}')

# set default files
def rename_pdf(root, theme):
    for f in root.glob(f'*.{theme}.pdf'):
        new_name = f.with_suffix('').with_suffix('.pdf')
        logger.info(f'renaming {f.name} -> {new_name.name}')
        f.rename(new_name)


def try_generate_pdf(f):
    try:
        generate_pdf(f)
    except Exception as e:
        logger.error(f'error inside a concurent loop: {e}')

def make_docx(final, theme):
    cwd = os.getcwd()

    for f in final:
        os.chdir(f.parent)

        theme = f.parent.parent / f'themes/{theme}.css'
        cmd = f'pandoc {f} -o {DOCX_ROOT / f.with_suffix(".docx").name} -c {theme}'
        shell_run(cmd)
        logger.info(cmd)
    
    os.chdir(cwd)

def go():

    # final = [path.join(RELEASE_ROOT,  f) for f in os.listdir(RELEASE_ROOT) if path.isfile(path.join(RELEASE_ROOT,  f)) and f.endswith('.html')]

    mds = RELEASE_ROOT / 'md'
    final = [f for f in mds.glob('*.md')]
    

    with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
        executor.map(try_generate_pdf, final)

    logger.info(f'{len(final)} pdf files generated.')

    default_print_theme = g_themes[0]
    logger.info(f'default_print_theme={default_print_theme}')

    rename_pdf(PDF_ROOT, default_print_theme)    
    make_docx(final, default_print_theme)


if __name__ == '__main__':
    go()

    #   var available_themes = {
    #   0: { 'name': 'cerulean', 'color': 'navbar-light bg-light' },
    #   1: { 'name': 'cerulean', 'color': 'navbar-dark bg-primary' },
    #   2: { 'name': 'cerulean', 'color': 'navbar-dark bg-dark' },
    #   3: { 'name': 'cosmo', 'color': 'navbar-light bg-light' },
    #   4: { 'name': 'cosmo', 'color': 'navbar-dark bg-primary' },
    #   5: { 'name': 'cosmo', 'color': 'navbar-dark bg-dark' },
    #   6: { 'name': 'custom', 'color': 'navbar-light bg-light' },
    #   7: { 'name': 'custom', 'color': 'navbar-dark bg-primary' },
    #   8: { 'name': 'custom', 'color': 'navbar-dark bg-dark' },
    #   9: { 'name': 'cyborg', 'color': 'navbar-light bg-light' },
    #   10: { 'name': 'cyborg', 'color': 'navbar-dark bg-primary' },
    #   11: { 'name': 'cyborg', 'color': 'navbar-dark bg-dark' },
    #   12: { 'name': 'darkly', 'color': 'navbar-light bg-light' },
    #   13: { 'name': 'darkly', 'color': 'navbar-dark bg-primary' },
    #   14: { 'name': 'darkly', 'color': 'navbar-dark bg-dark' },
    #   15: { 'name': 'flatly', 'color': 'navbar-light bg-light' },
    #   16: { 'name': 'flatly', 'color': 'navbar-dark bg-primary' },
    #   17: { 'name': 'flatly', 'color': 'navbar-dark bg-dark' },
    #   18: { 'name': 'journal', 'color': 'navbar-light bg-light' },
    #   19: { 'name': 'journal', 'color': 'navbar-dark bg-primary' },
    #   20: { 'name': 'journal', 'color': 'navbar-dark bg-dark' },
    #   21: { 'name': 'litera', 'color': 'navbar-light bg-light' },
    #   22: { 'name': 'litera', 'color': 'navbar-dark bg-primary' },
    #   23: { 'name': 'litera', 'color': 'navbar-dark bg-dark' },
    #   24: { 'name': 'lumen', 'color': 'navbar-light bg-light' },
    #   25: { 'name': 'lumen', 'color': 'navbar-dark bg-primary' },
    #   26: { 'name': 'lumen', 'color': 'navbar-dark bg-dark' },
    #   27: { 'name': 'lux', 'color': 'navbar-light bg-light' },
    #   28: { 'name': 'lux', 'color': 'navbar-dark bg-primary' },
    #   29: { 'name': 'lux', 'color': 'navbar-dark bg-dark' },
    #   30: { 'name': 'materia', 'color': 'navbar-light bg-light' },
    #   31: { 'name': 'materia', 'color': 'navbar-dark bg-primary' },
    #   32: { 'name': 'materia', 'color': 'navbar-dark bg-dark' },
    #   33: { 'name': 'minty', 'color': 'navbar-light bg-light' },
    #   34: { 'name': 'minty', 'color': 'navbar-dark bg-primary' },
    #   35: { 'name': 'minty', 'color': 'navbar-dark bg-dark' },
    #   36: { 'name': 'pulse', 'color': 'navbar-light bg-light' },
    #   37: { 'name': 'pulse', 'color': 'navbar-dark bg-primary' },
    #   38: { 'name': 'pulse', 'color': 'navbar-dark bg-dark' },
    #   39: { 'name': 'sandstone', 'color': 'navbar-light bg-light' },
    #   40: { 'name': 'sandstone', 'color': 'navbar-dark bg-primary' },
    #   41: { 'name': 'sandstone', 'color': 'navbar-dark bg-dark' },
    #   42: { 'name': 'simplex', 'color': 'navbar-light bg-light' },
    #   43: { 'name': 'simplex', 'color': 'navbar-dark bg-primary' },
    #   44: { 'name': 'simplex', 'color': 'navbar-dark bg-dark' },
    #   45: { 'name': 'sketchy', 'color': 'navbar-light bg-light' },
    #   46: { 'name': 'sketchy', 'color': 'navbar-dark bg-primary' },
    #   47: { 'name': 'sketchy', 'color': 'navbar-dark bg-dark' },
    #   48: { 'name': 'slate', 'color': 'navbar-light bg-light' },
    #   49: { 'name': 'slate', 'color': 'navbar-dark bg-primary' },
    #   50: { 'name': 'slate', 'color': 'navbar-dark bg-dark' },
    #   51: { 'name': 'solar', 'color': 'navbar-light bg-light' },
    #   52: { 'name': 'solar', 'color': 'navbar-dark bg-primary' },
    #   53: { 'name': 'solar', 'color': 'navbar-dark bg-dark' },
    #   54: { 'name': 'spacelab', 'color': 'navbar-light bg-light' },
    #   55: { 'name': 'spacelab', 'color': 'navbar-dark bg-primary' },
    #   56: { 'name': 'spacelab', 'color': 'navbar-dark bg-dark' },
    #   57: { 'name': 'superhero', 'color': 'navbar-light bg-light' },
    #   58: { 'name': 'superhero', 'color': 'navbar-dark bg-primary' },
    #   59: { 'name': 'superhero', 'color': 'navbar-dark bg-dark' },
    #   60: { 'name': 'united', 'color': 'navbar-light bg-light' },
    #   61: { 'name': 'united', 'color': 'navbar-dark bg-primary' },
    #   62: { 'name': 'united', 'color': 'navbar-dark bg-dark' },
    #   63: { 'name': 'yeti', 'color': 'navbar-light bg-light' },
    #   64: { 'name': 'yeti', 'color': 'navbar-dark bg-primary' },
    #   65: { 'name': 'yeti', 'color': 'navbar-dark bg-dark' }
    # }
