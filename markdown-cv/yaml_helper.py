import re

import logging
logger = logging.getLogger('.'.join(['cv', __name__]))

import yaml

class YamlHelper:

    HEADER_RE = re.compile(r'^[^-\w]*?---$.+?^---$', re.MULTILINE|re.DOTALL)

    @staticmethod
    def strip_yaml(s):

        s, n = YamlHelper.HEADER_RE.subn('', s, 0)

        logger.debug(f'{n} yaml headers removed') 

        return s  

    @staticmethod
    def find_all(s):

        return  YamlHelper.HEADER_RE.findall(s)

    @staticmethod
    def yaml_to_string(header_block):
        return f'''---
{yaml.dump(header_block, default_flow_style=False, allow_unicode=True)}
---
'''
    @staticmethod
    def split_md(text):
    
        header, body = {}, text.lstrip()

        yaml_start = '---'
        if(text.startswith(yaml_start + '\n')):
            content = text.lstrip().split(yaml_start, 2)
            if len(content) == 3:
                header = yaml.load(content[1])
                body  = content[-1]
        return header, body
