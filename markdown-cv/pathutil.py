import os
from os import path

#
# path utils
#

def this_folder():

    return path.dirname(path.abspath(__file__))

def par_folder():

    return path.join(this_folder(), '..')

def make_if_none(directory):

    if not path.exists(directory):
        os.makedirs(directory)
