#! 
from datetime import date   
import os     
from os import listdir, path
import shutil
from pathlib import Path

import logging
logging.basicConfig(level=logging.DEBUG, format='[%(asctime)s|%(filename)s|%(levelname)s]: %(message)s', datefmt='%d/%b/%Y %H:%M:%S')

#
# collect theme css to the same folder with script
#
def fix_google_fonts(src, dest):
    src =  Path(src)
    orig = src.read_text()

    import re

    m = re.search(r'url\("(http[^)]+)"\)', orig)
    if m: 
        logging.debug(f'{src.name}: {m.group(1).split("?", 1)[-1]}')


    # orig = orig.replace('googleapis.com', 'lug.ustc.edu.cn')
    # https://sb.sb/blog/css-cdn/
    orig = orig.replace('fonts.googleapis.com', 'fonts.loli.net')
    
    Path(dest).write_text(orig)

import shutil

def copyfile(frm, to):
    shutil.copyfile(str(frm), str(to))
    # to = to.relative_to(frm)
    logging.debug(f'{frm.name} -> {to}')

def collect(root):
    from pathlib import Path

    root = Path(root)
    target = Path(__file__).absolute().parent / 'template/themes'
    target.mkdir(exist_ok=True)

    for t in root.iterdir():
        css = t / 'bootstrap.min.css'
        new_css = target / (t.name +'.css')
        if css.exists():
            copyfile(css, new_css)
            new_zh_css = new_css.with_name('zh-' + new_css.name)
            fix_google_fonts(str(new_css), str(new_zh_css))
   

            vue_cv = Path(__file__).absolute().parent.parent/'vue-cv/src/assets/bootswatch-4'

            copyfile(new_css, vue_cv/new_css.with_suffix('.bs4').name)            
            copyfile(new_zh_css, vue_cv/new_zh_css.with_suffix('.bs4').name)       

                 

try:             
    collect('/home/ttan/repo/public/bootswatch/dist')
except:
    collect(r'F:/bootswatch/dist')

