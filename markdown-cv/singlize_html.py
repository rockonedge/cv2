import os
from os import path
import re
import concurrent.futures

from datetime import datetime

import logging
logger = logging.getLogger('.'.join(['cv', __name__]))

#
# path utils
#

from pathutil import this_folder, par_folder, make_if_none

def solidify_html():
    def read_files_by(root, ext):

        def read(p):        
            with open(p, encoding='utf-8') as f:
                return f.read()

        def list_files():
            def list_files_recursively(root):
                return [os.path.join(r, f) for r, d, ff in os.walk(root) for f in ff]

            def skip(f):
                f = f.lower()
                return  False
            
            def ext_files():
                return [f for f in list_files_recursively(root) if f.lower().endswith(ext) and not skip(f)]
            
            return ext_files()

        return {os.path.basename(f): read(f) for f in list_files()}

    def base64fy(pics_root):

        import base64
        from urllib.parse import quote

        final = {}
        imgs = [os.path.join(r, f) for r, d, ff in os.walk(pics_root) 
                for f in ff 
                    if f.lower().endswith('jpg') or f.lower().endswith('png') or f.lower().endswith('svg')]

        for img in imgs:
            name = os.path.basename(img)
            format = name.rpartition('.')[-1]

            if format.endswith('svg'):
                format = 'svg+xml;utf8'
                with open(os.path.join(pics_root, img), 'r', encoding='utf-8') as f:              
                    data = quote(f.read())
            else:
                format = f'{format};base64'
                with open(os.path.join(pics_root, img), 'rb') as f:  
                    data = base64.b64encode(f.read()).decode('utf-8')

            final[name] = f'data:image/{format},{data}' 
        return final



    def html_folder(html):
        import os
        return os.path.dirname(os.path.abspath(html))

    root_foler = html_folder('index.html')
    css = read_files_by(root_foler, '.css')
    js = read_files_by(root_foler, '.js')
    images = base64fy(root_foler)
    def replace_css(line):
        if '<link ' in line.lower() and 'href=' in line.lower(): 
            for k, style in css.items():
                if k in line:
                    logger.debug(f'replacing {line.strip()}')
                    line = f'\n<!--start of {k}-->\n<style>{style}</style>\n<!--end of {k}-->\n'
                    break
        return line

    def replace_js(line):

        if '<script ' in line.lower() and '</script>' in line.lower():
            for k, script in js.items():
                if k in line:
                    logger.debug(f'replacing {line.strip()}')
                    line = f'\n<!--start of {k}-->\n<script>{script}</script>\n<!--end of {k}-->\n'
                    break
        return line

    def replace_img(line):
        # if line.lstrip().startswith('<img ') and 'src=' in line and not 'data:' in line:
        for k, image in images.items():
            if k in line:
                logger.debug(f'replacing {line.strip()}')
                markdown_img = r'!\[(.*?)\]\((.*?)\)'
                line = re.sub(markdown_img, f'<img src="{image}" />', line)
                # line = re.sub('src=\S+', f'src="{image}"', line)
                # ![](./pics/husky-contribution.jpg)
                line = f'\n<!--start of {k}-->\n{line}\n<!--end of {k}-->\n'
                break
        return line

    def replace_one(src, dest):
        out = []
        with open(src, encoding='utf-8') as f:
            for line in f.readlines():

                line = replace_css(line)
                line = replace_js(line) 
                line = replace_img(line)

                out.append(line)

                # logger.debug(out)

        with open(dest, 'w+', encoding='utf-8') as f:
            f.write(''.join(out))
    #
    #   apply to all lines
    #

    def run():
        root_input = path.join(this_folder(), 'release')
        root_output = path.join(root_input, 'all-in-one-html')
        make_if_none(root_output)

        def run_one_file(f):
            if f.lower().endswith('.html'):
                src = path.join(root_input, f)
                dest = path.join(root_output, f)        
                replace_one(src, dest)
                logger.info(f'done with {dest}')
                
        with concurrent.futures.ThreadPoolExecutor(max_workers=8) as executor:
            executor.map(run_one_file, os.listdir(root_input))


    start = datetime.now()   
      
    run()

    # logger.info('{} files in total.'.format(len(os.listdir(root_input))))
    logger.info(datetime.now() - start)
    
    # logger.debug(css.keys())
    # logger.debug(js.keys())
    # logger.debug(images.keys())

def go():         
    solidify_html()

if __name__ == "__main__":
    go()










