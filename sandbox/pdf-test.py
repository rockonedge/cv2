import subprocess
import logging
import sys

datefmt='%d/%b/%Y %H:%M:%S'

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
# logger.setLevel(logging.INFO)

# create console handler with a higher log level
# http://stackoverflow.com/questions/1383254/logging-streamhandler-and-standard-streams

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
ch.setFormatter(logging.Formatter("[%(levelname)s|%(asctime)s|%(filename)s|%(thread)d]:  %(message)s", datefmt=datefmt))
logger.addHandler(ch)

def shell_run(command):
    import subprocess
    logger.debug(command)
    print(command)
    cp = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    # logger.debug(f'args={cp.args} , returncode={cp.returncode} stdout={cp.stdout}, stderr={cp.stderr}')
    print(f'args={cp.args} , returncode={cp.returncode} stdout={cp.stdout}, stderr={cp.stderr}')

from concurrent.futures import ThreadPoolExecutor

with ThreadPoolExecutor() as executor:
    files = []
    for r in range(0, 66, 3):
        files.append(f'wget https://ttan-beta.netlify.com/pdf/mini-cv-zh.{r}.pdf')
        files.append(f'wget https://ttan-beta.netlify.com/pdf/full-cv-zh-en.{r}.pdf')
    executor.map(shell_run, files)

    # u = f'https://ttan-beta.netlify.com/pdf/mini-cv-zh.{r}.pdf'
    # # shell_run([r'C:\Program Files (x86)\Opera\launcher.exe', u])
    # # shell_run([r'firefox', u])
    # # shell_run([r'wget', u])
    # (f'wget {u}')