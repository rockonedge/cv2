import os
import re



def html(where):
    def read_files_by(root, ext):

        def read(p):        
            with open(p, encoding='utf-8') as f:
                return f.read()

        def list_files():
            def list_files_recursively(root):
                return [os.path.join(r, f) for r, d, ff in os.walk(root) for f in ff]

            def skip(f):
                f = f.lower()
                return  False
            
            def ext_files():
                return [f for f in list_files_recursively(root) if f.lower().endswith(ext) and not skip(f)]
            
            return ext_files()

        return {os.path.basename(f): read(f) for f in list_files()}

    def base64fy(pics_root):

        import base64
        from urllib.parse import quote

        final = {}
        imgs = [os.path.join(r, f) for r, d, ff in os.walk(pics_root) 
                for f in ff 
                    if f.lower().endswith('jpg') or f.lower().endswith('png') or f.lower().endswith('svg')]

        for img in imgs:
                name = os.path.basename(img)
                format = name.rpartition('.')[-1]

                if format.endswith('svg'):
                    format = 'svg+xml;utf8'
                    with open(os.path.join(pics_root, img), 'r', encoding='utf-8') as f:              
                        data = quote(f.read())
                else:
                    format = f'{format};base64'
                    with open(os.path.join(pics_root, img), 'rb') as f:  
                        data = base64.b64encode(f.read()).decode('utf-8')

                final[name] = f'data:image/{format},{data}' 
        return final

    def html_folder(html):
        import os
        return os.path.dirname(os.path.abspath(html))

    root_foler = html_folder('index.html')
    css = read_files_by(root_foler, '.css')
    js = read_files_by(root_foler, '.js')
    images = base64fy(root_foler)
    def replace_css(line):
        if '<link ' in line.lower() and 'href=' in line.lower(): 
            for k, style in css.items():
                if k in line:
                    print(f'replacing {line}')
                    line = f'\n<!--start of {k}-->\n<style>{style}</style>\n<!--end of {k}-->\n'
                    break
        return line

    def replace_js(line):

        if '<script ' in line.lower() and '</script>' in line.lower():
            for k, script in js.items():
                if k in line:
                    print(f'replacing {line}')
                    line = f'\n<!--start of {k}-->\n<script>{script}</script>\n<!--end of {k}-->\n'
                    break
        return line

    def replace_img(line):
        
        if line.startswith('<img ') and 'src=' in line:
            for k, image in images.items():
                if k in line:

                    print(f'replacing {line}')
                    line = re.sub('src=\S+', f'src="{image}"', line)
                    line = f'\n<!--start of {k}-->\n{line}\n<!--end of {k}-->\n'
                    break
        return line
    def replace_all():
        out = []
        with open(where, encoding='utf-8') as f:
            for line in f.readlines():

                line = line.strip()
                line = replace_css(line)
                line = replace_js(line) 
                line = replace_img(line)

                out.append(line)

                # print(out)

        with open('out.' + where, 'w+', encoding='utf-8') as f:
            f.write(''.join(out))
    #
    #   apply to all lines
    #
    replace_all()
    
    print(css.keys())
    print(js.keys())
    print(images.keys())

    
html('index.html')
