# from SSH - the definitive guide

## ~ or $HOME
A user’s home directory on a Unix machine, particularly when used in a file path
such as ~/ filename. Most shells recognize ~ as a user’s home directory, with the
notable exception of the Bourne shell. `$HOME`  is recognized by all shells

## Using keys
Using keys, together with a program called an authentication agent, SSH can authen-ticate you to all your computer accounts securely without requiring you to memorize many passwords or enter them repeatedly. It works like this:
1. In advance (and only once), place special, nonsecure files called
public key files into your remote computer accounts. These enable your SSH clients (
ssh,  scp  )to  access your remote accounts.  
2. On  your  local  machine,  invoke  the  ssh-agent  program,  which  runs  in  the  background.  
3. Choose the key (or keys) you will need during your login session.  
4. Load the keys into the agent with the ssh-add program. This requires knowledge  of each key’s secret passphrase.  
At this point, you have an  ssh-agent  program running on your local machine, hold-ing your secret keys in memory. You’re now done. You have passwordless access to all  your  remote  accounts  that  contain  your  public  key  files.

## SSH vs. PGP
SSH incorporates some of the same encryption algorithms as PGP and GnuPG, but
applied  in  a  different  way.  PGP  is  file-based,  typically  encrypting  one  file  or  email
message at a time on a single computer. SSH, in contrast, encrypts an ongoing ses-
sion  between  networked  computers.  The  difference  between  PGP  and  SSH  is  like
that between a batch job and an interactive process.

## SSH vs. Kerberos
Kerberos and SSH solve similar problems but are quite different in scope.

Kerberos authenticates users by way of tickets, small sequences of bytes with limited lifetimes, while user passwords remain secure on a central machine.

- Kerberos  ensures  that users’ passwords travel on the network as little as possible and are stored only on the central host. 

- SSH sends passwords across the network (over encrypted connections, of course) on each login and stores keys on each host from which SSH is used.

## SSL
An SSL participant proves its identity by a digital certificate, a set of cryptographic data.  A  certificate  indicates  that  a  trusted  third  party  has  verified  the  binding
between  an  identity  and  a  given  cryptographic  key.  Web  browsers  automatically
check the certificate provided by a web server when they connect by SSL, ensuring
that  the  server  is  the  one  the  user  intended  to  contact.  Thereafter,  transmissions
between the browser and the web server are encrypted.

## Stunnel
stunnel is an SSL tool created by Micha Trojnara of Poland. It adds SSL protection to existing TCP-based services in a Unix environment, such as POP or IMAP servers, without requiring changes to the server source code. It can be invoked from inetd as a wrapper for any number of service daemons or run standalone, accepting networkn connections itself for a particular service.

stunnel performs authentication and authorization  of  incoming  connections  via  SSL;  if  the  connection  is  allowed,  it  runs  the server  and  implements  an  SSL-protected  session  between  the  client  and  server programs.

This is especially useful because certain popular applications have the option of running some client/server protocols over SSL. For instance, email clients like Microsoft Outlook and Mozilla Mail can connect to POP, IMAP, and SMTP servers using SSL. For more stunnel information, see http://www.stunnel.org/