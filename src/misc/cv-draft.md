
仪器·仪表·工业自动化·电气 | Global Engineering - Controls Team Leader | 6年工作经验 | 硕士 | 上海


## 谭智 <span class="en">(Tom Gee)</span> <span class="bas">( 男, 31岁 )</span>

### 基本资料

![
          个人照片
        ](cn-utf8_files/mht32DD(1).tmp)

电子邮箱：[rockonedge@gmail.com](mailto:rockonedge@gmail.com)

手机：13661876385

出生日期：1977年9月

户口所在地：上海


### 自我评价/职业目标

#### 自我评价：

9年硬件电子，软件研发经验，领域涉及工业控制，自动化测量，实时嵌入式系统。  

4年组建，领导和激励技术研发团队的成功经验。崇尚以身作则“Lead by example"  

6年行业领先大型跨国公司工作经历，善于多文化环境下沟通合作，  

工作自觉性与责任感强。  

长久敏锐的技术敏感性，对新知识和挑战充满渴望。  

#### 职业目标：

跨国公司研发中心经理或其他团队领导职位，主持嵌入式系统，工业控制与自动化系统研发工作

### 职业概况/求职意向

*   现从事行业：仪器·仪表·工业自动化·电气
*   现从事职业：科研类
*   工作经验：6年工作经验
*   现职位级别：高级职位(非管理类)
*   期望薪水：面议

*   期望从事行业：

    电子·微电子 | 仪器·仪表·工业自动化·电气 | 能源（电力·石油）·水利

*   期望从事职业：

    自动控制 | 科研类 | 高级软件工程师 | 研发工程师 | 部门经理

*   期望工作性质：全职
*   期望工作地区：上海，重庆，青岛
*   到岗时间：面谈
*   海外工作经历：有

### 工作经验


#### 赫斯基注塑系统（上海）有限公司

外商独资．外企办事处| 100 - 499人

#### 职位：Global Engineering - Controls Team Leader（2005年11月 - 现在）

工作地点：上海  
汇报对象：Engineering Manager(Canada)  
下属人数：7人  
工作职责和业绩：

1\. 组建全新Global Engineering Controls software中国团队，并迅速证明其价值，从而使团队规模翻番。  

2\. 培训、领导团队参与全球（Bolton(Canada), Luxemburge, Detroit(US),上海）范围内技术支持，包括：  

- 现有控制软件系统软进行1）维护性升级和2）根据用户需求定制性开发。涉及EtherCAT, Profibus Euromap等总线协议，大量C++，C++, ASP.net, VB以及IEC61131 PLC代码修改与编写。比如多语言支持，第三方机器人(e.g. ABB Robot)整合等。  

- 新系统产品研发，比如New HMI, Fusion等。

#### ST意法半导体（上海）有限公司

电子·微电子| 外商独资．外企办事处| 100 - 499人

#### 职位：Embedded System Solutions Team Leader,Senior Application Eng |MPG技术应用中心（2003年9月 - 现在）

工作地点：上海  
汇报对象：MPG技术应用中心经理，Division 经理（法国）  
下属人数：4人  
工作职责和业绩：

作为Team Leader 先后领导Serial Non-Volatile Memory Division Support Team和Embedded System Solutions Team.作为公司R&D力量在亚太地区的延伸, 代表Serial Non-Volatile Memory Division领导和管理一个团队负责整个亚太地区应用开发：  

(1).开发基于MCU,DSP,ARM7/9等CPU的嵌入式应用参考设计（包括软件代码编写与硬件验证平台），演示EEPROM/FLASH等的功能和应用领域。  
(2). Flash存储器产品跨平台，可复用驱动代码编写与验证.  
(3). Flash存储器芯片功能验证，Datasheet 与Application Notes编写.  
(4). PC工具软件编写.  

主导开发了基于uPSD（Intel8032内核）的BIOS Memory参考设计；  
基于uPSD（Intel8032内核）的Serial Flash参考设计；  
基于ST7（Motorola内核）的Serial Flash Programmer等；  
移植uCLinux于STR71x。  
移植uC/OS于STR71x。  
基于XML与ADO用Visual C++ 2003.net开发了一套项目管理软件(约50,000行代码)，用于跟踪同步上海、意大利、法国之间的项目进度。  

参与开发了基于ARM7/9的NOR/NAND FLASH通用开发平台。

#### 施耐德电气（中国）投资有限公司

外商独资．外企办事处| 50 - 99人

#### 职位：设计工程师 - 工业控制与自动化 |中国研发中心（2002年4月 - 2003年8月）

工作地点：上海  
汇报对象：研发中心经理  
下属人数：3人  
工作职责和业绩：

(1). 新产品电气/电子硬件设计.  
(2). 为实验室设计开发软/硬件设备/工具.  
(3). 根据IEC/GB相关标准设计新产品的检验/验收试验方案及标准.  
(4). 根据IEC/GB 对新产品 / 样品进行设计阶段预试验.  
(5). 国产化设计.  
(6). 对实验室技术人员技术培训.  
(7). 为合资工厂提供高级技术支持.  

完成包括领导开发基于NEZA PLC的新一代auto-transfer system；基于MCU和PID算法设计了恒温试验控制箱，采用VC+ADO为实验室开发了一套基于关系数据库的国产化试验管理软件。

#### 哈尔滨工业大学车辆电器研究所

教育·培训·科研·院校| 事业单位| 1 - 49人

#### 职位：科研人员，在读研究生（1999年4月 - 2002年1月）

工作地点：哈尔滨  
汇报对象：所长  
下属人数：2人  
工作职责和业绩：

1). 工业控制与自动化设备研究与开发.  
(2). 实时控制与测量方法和可靠性的研究.  
(3). 直接数字控制系统 (Direct Digital Control System), 集散控制系统 (DCS)，现场总线控制系统(FCS)等工与自动化系统的研究.  
(4). 实时嵌入式系统 (RTOS) 的应用研究与开发.  
(5). 接口电路与总线协议研究.  

承担研究所的多项分别基于工控机（IPC）、单片机(MCU)的硬件系统设计及实时数据采集、处理和数据管理软件开发（采用C,C++ 及VB）。  

受哈尔滨铁路局委托，先后成功开发“电子镇流器自动化测试系统”，“低压电器自动化测试系统”，并被广泛推广于齐齐哈尔等7个车辆段，后者经黑龙江省科委专家组技术鉴定“居于国内领先”。  

独立承担了此二项目全部全部软/硬件设计，包括ISA I/O, PCI A/D 等计算机板卡设计，电源及负载的数字化设计与控制；EMI, 谐波干扰处理等。  

相关论文发表于第八届国际电器可靠性会议。

离职/转岗原因：硕士毕业







### 项目管理经验





#### 2006年6月 - 2008年6月

#### 项目名称：HyMET HeatLogger

软件环境：WTL/ATL, C++, POCO  
硬件环境：PC  
开发工具：Visual Studio 2005  
项目描述：

监测，记录HyMET注塑机各加热部件数据，供1)设计工程师验证设计，2)解决客户现场故障。  
技术要求包括：  
- 同时采集通道数可多达100，采集频率在1s-180s范围内可调。  
- 因为程序安装于全球客户生产现场，由Husky服务工程师每3个月至6个月取回数据供设计工程师分析， 1)程序必须占用尽可能少的系统资源 2）程序不能被客户意外终止 3）程序能自动监测注塑机运行/停止状态，智能停止或重新启动记录，并保证记录数据不能因为意外掉电而丢失。4）预测记录数据大小，以保证在无人监控时导致记录空间不足。  


项目中职责：

Team Leader  
设计解决方案，并编写界面（http://thezebra.spaces.live.com/blog/cns!AE99BCB7CCAAB5A8!300.entry）作为范例向组员讲解Windows GUI设计相关知识。

#### 2007年11月 - 2008年1月

#### 项目名称：HMI Re-Architecture

软件环境：Net 2.0, C#, C++  
硬件环境：PC  
开发工具：Visual Studio 2005  
项目描述：

由于1）现有控制系统已经使用多年，演变的庞大而难以维护；2）作为公司第一代基于PC而不是PLC的控制系统，软件构架上不成熟而难以扩展；3）软件工程思想和开发工具的发展使得重构原有系统变得可能而且代价低廉。  

基于.NET和C#，重构原有控制系统HMI部分。

项目中职责：

Team Leader & Project co-Leader.  
参与确认软件架构，确保TDD开发过程实施，确保开发进度和代码质量。

#### 2006年11月 - 2007年7月

#### 项目名称：多语言翻译管理系统

软件环境：SQL Server 2005, C#, Windows Server 2003  
硬件环境：PC  
开发工具：Visual Studio 2005  
项目描述：

为了支持多达23种语言HMI显示，开发人员在开发控制系统软件时涉及到任何字符串均需，事先向专职人员申请并获得特定ID替代原字符串用于开法中，以支持用户使用时界面语言动态翻译。  

该网站的开发使得原来的人工过程自动化，申请新字符串ID的处理时间从原来大约2星期减少到实时获得ID，并能提供更多上下文信息用于提高翻译准确性。还提供了其他查询管理高级新功能。  



项目中职责：

Team & Project Leader。  
在尊重开发人员自主性的情况下，把握项目的总体技术架构，完成质量和进度。协助上海的加拿大的工程师沟通。保确保测试驱动开发方法(TDD)的正确实施。

#### 2004年5月 - 2004年10月

#### 项目名称：SPI 总线串行Flash应用包

项目描述：

设计硬件，并编写调试了通过SPI接口访问M25P系列程序存储器，M45PE系列数据存储器的代码.演示字符集存储，与音频存储等应用。  
为即将推出的Soc产品 uPSD3300提供开发包,

项目中职责：

负责全部硬件与软件设计

#### 2003年12月 - 2004年3月

#### 项目名称：Firmware Hub/Low Pin Count BIOS flash 存储器功能验证演示平台

项目描述：

为新推出的M50系列PC BIOS 专用Flash存储器，提供功能验证平台，及用户开发演示代码。包括2方面开发:  
1\. 功能验证和代码演示的硬件平台;  
2\. 分成2层的驱动代码（C语言）.  
- 顶层（协议层）: 提供Firmware Hub/Low Pin Count 协议的封装，以标准C编写，独立于硬件设计.  
- 底层（物理层）: 提供硬件平台相关的地址，数据，控制相关基本操作代码，与框架.

项目中职责：

与芯片设计工程师合作, 全部负责软、硬件设计

#### 2003年9月 - 2003年12月

#### 项目名称：Serial Flash Programmer Tool Kit

项目描述：

In line with Company’s new effort to provide more e-tools for mass market, this kit is to facilitate their use of ST memory product. It consists of two parts: PC-based software and a circuit board(hardware) connected to PC through the parallel port (LTP) or USB. The customer can test and program the memory chip on any PC running Windows 9x, 2K,XP. Interface protocols (SPI, I2C etc.) are implemented in software.  
Visual C++, Protel, Numega Diver Suite, and DDK

项目中职责：

full responsibility

#### 2002年11月 - 2003年6月

#### 项目名称：Toro Project (a.k.a ATS project)

项目描述：

领导一跨部门小组（研发中心，市场部，现场支持部）开发新一代auto-transfer system，以提供更智能，更友好的人机界面，更高可靠性和更低廉的成本.此系统基于NEZA PLC，配合NS系列塑壳式断路器使用, 为医院等场所提供可靠的主备用电源管理，以持续电源供应. 本人领导并推动此项目，先后设计出3台样品，拟定并完成了各项IEC/GB规定相关试验，完成了投资/收益分析，技术可行性分析及设计方案。此项设计节省了1/3的成本。

项目中职责：

项目领导

#### 2000年9月 - 2001年4月

#### 项目名称：断路器接触器综合测试系统

项目描述：

此课题应哈尔滨铁路局要求设计，为铁道车辆用电器（断路器，接触器，热继电器）的常规检修提供统一的自动化平台,检修内容包括接触电阻，闭合/断开时间，过压/欠压/失压，过流，短路等电气性能试验和正弦振动等机械性能试验.  

基于工业控制计算机。此系统.  
硬件设计包括:  
·工业控制计算机ISA I/O 板卡设计  
·工业控制计算机PCI A/D 板卡设计  
·计数器/定时器电路设计  
·大电流采样与切换电路  
·8421 - 编码电源，提供 1A to 1,300A 电流 及440VAC/220VDC电压  
·高精度阻性负载设计 (分辨率: 1/512 ohm)  

软件设计（采用Visual C++）包括:  
·精确电压/电流闭环控制算法  
·友好、全面的运行参数显示  
·故障报告与寿命预测  
·测试数据管理  
此项目深受用户好评，以每套单价14万元，被广泛推广至齐齐哈尔等7个车辆段，于2002年 12月获省教育厅科技进步一等奖，2003年5月获省科技厅科技进步二等奖在国家级学术会议发表相关论文一篇。  


项目中职责：

作为硕士学位论文课题，承担了全部的项目前期调研，软/硬件设计及少量机械设计。

#### 1999年4月 - 1999年10月

#### 项目名称：电子镇流器综合测试系统

项目描述：

此课题应哈尔滨铁路局要求设计， 此系统能同时测试多达八只电子各种型号镇流器/逆变器 (15W, 20W, 30W, 40W)，提供各种详细性能参数信息，包括灯电压，灯电流， 功率因数，谐波畸变，镇流器温升等. 实时显示电压，电流波形。一旦发现故障，还将提供故障定位与诊断帮助. 建立试品维修历史数据库，并自动生成报表.  
硬件设计包括:  
·ISA I/O 板, A/D 板  
·交/直流电源 (DC 47V-57V, AC 200V – 240V)  
·传感器板 (电压, 电流 和 温度检测 )  
·试品切换板  
软件设计包括:  
·硬件驱动  
·实时数据采样、分析 (快速傅立叶变换)  
·波形显示  
·测试数据管理该系统以其高效准确的性能深受好评，并被推广至齐齐哈尔等铁路分局.  

在国家级学术期刊发表相关论文一篇.

项目中职责：

作为学士学位论文课题。负责了的调研，全部硬件设计，软件设计文档和调试。









### 教育背景





#### 哈尔滨工业大学（1999年9月 - 2002年3月）

电气工程| 电气信息类| 硕士 |哈尔滨  

#### 哈尔滨工业大学（1995年9月 - 1999年7月）

电气工程| 电气信息类| 本科 |哈尔滨  








### 培训经历





#### 2008年8月  
高校人士的7个习惯

培训时长：2  
城市：上海  
培训机构：Right Managament  

#### 2006年1月  
Team Leadership development Training

培训时长：1 周  
城市：Toronto  
培训机构：Workplace Competence International Limited  
培训描述：

Team Leardership skills.

#### 2004年5月  
Technical training on Serial Non volative memories

培训时长：2周  
城市：Rousset  
培训机构：STMicroelectronics, Serial Non-Volatile Memory Division  
培训描述：

SCOPE:  
Initial product training and application familiarization.  
Coverage of Business aspects.  
Introduction to the Division operations, Design, Product  
engineering,Production engineering, Planning, Quality  
assurance aspects.Communication tools, Intranet, Internet  
where what to find.  

PRODUCT Coverage:  
Serial EEPROM, Serial Flash, BIOS Flash, Contactless  
Memories(RFID)

#### 2002年8月  
商务英语考试

培训时长：6个月  
城市：上海  
培训机构：ClarkMorgan consulting  

#### 2002年7月  
Creative Thinking and Problem Solving

培训时长：1周  
城市：上海  
培训机构：Key Consulting  








### 在校期间信息



奖学金及其他奖励：

多次获得人民奖学金。

校内校外活动：

先后工作于院学生会与校学生会。









### 语言及方言





*   英语：熟练

语言水平描述：







### 职业技能与特长



特长：  
* 板级原理图、PCB 设计经验，熟念使用Protel等EDA工具, ISA/PCI板卡设计经验。  
* 使用Matlab, Pspice 仿真经验.  
* 基于单片机，ARM, DSP的嵌入式系统设计经验.  

* 多年在工业控制计算机（Industrial PC）、嵌入式(embedded)与桌面(PC)应用领域，采用C/C++语言基于STL，BOOST Library，MFC，ATL等库应用开发经验。使用工具包括Visual C++,NI Labview 等开发工具。  
* 对面向对象（OOP），范式程序设计（GP）及各种设计模式有深入研究。  
* 采用 C++/CLI, C# 基于.NET框架应用开发经验。  
* XML使用经验。  
* ADO 数据库开发经验 。  
* SOCKET编程经验，ACE（Adaptive Communication Environment）使用经验。  
* 熟悉网络通讯，PLC 及现场总线控制系统 (FCS).  
* 熟悉多种串行/并行接口与通讯协议，包括RS232, RS485, USB, SPI, I2C, SPP, EPP, ECP, Low Pin Count/Firmware Hub开发经验等  
* 熟悉uC/OS, ecos, uCLinux等嵌入式操作系统（RTOS）.  
* Windows内核实时驱动与控制程序开发经验,有WinDrive 和 Numega DriverWorks等工具使用经验。







### 证书





#### 2007 年

#### 杰出贡献证书

说 明：

由赫斯基亚太总经理和全球副总裁共同签署的杰出贡献证书。









### 著作/论文





* 研究生攻读期间在国家级期刊与国际会议发表2 篇学术论文。  

1 翟国富, 谭智, 陈 伟.铁路客车电子镇流器逆变器综合试验台的研制.低压电器  
2 翟国富, 谭智等.铁路客车断路器接触器性能及可靠性测试系统的研究.第8届国际电器可靠性会议论文集  









### 兴趣爱好





* 读书、音乐、跑步、自行车、旅游  









### 社区参与/贡献





* 博客：http://thezebra.spaces.live.com/  
* Poco Project 贡献者(http://pocoproject.org/poco/info/contributors.html)  
* 发表  
1\. http://www.codeproject.com/cpp/LiteralConversion.asp  

A helper class to convert integer literals between decimals, octals, binaries and hexadecimals.  





















# Tom Tan

E-mail: tom.tan@live.com
Mobile: 13524826926


## Career Objectives: 

## Working Experience
### Husky Injection Molding Systems(Shanghai) Ltd
Foreign Company / Rep. Office| 100 - 499

Position: Controls Team Leader |Global Engineering(2005/11 - Present)
Location: Shanghai
Reporting to: Controls Manager(Canada), Global Engineering Manager(Shangh)
Number of People under My Supervision: 7 People
Responsibilities & Achievements: 
- Top Team leader acccording to global employee survey conducted by [Kenexa]() 
- bianque , 
- Led the implementation of [Shotscope NX](http://www.husky.co/EN-US/Shotscope-NX.aspx), 
```
The industry’s most advanced plant-wide process and production monitoring system
```, check it out at its [homepage](http://www.husky.co/EN-US/Shotscope-NX.aspx)
- migrated Polaris HMI from VB/COM to C#/.NET 
- TWinCAT-based  PLC system 
- all *system* productlines launched in the last decade Hylectric, HyPET, HyPET HPP, HyCAP, HyperSync, Barrier injection machines
- Altanium  HotRunner Controller, software mostly
- integration of Altanium and IMM

-  Set up from scratch a brand new Global Engineering Controls software team of 5 people, which quickly gained trust and soon expanded to be 7 for more assignements being move from the headquarter in Canada.


-  Train and lead the team to provide controls system enhancement and other R&D technical activities for all global sites, including Bolton(Canada), Luxemburge, Detroit(US),Shanghai:

  - Lead the team to 1) maintain and upgrade the current control system in use and 2) design customized features for customers both in PLC and HML, which involves such fieldbus protocols as EtherCAT, Profibus Euromap，and programming languages  such as C++, ASP.net, VB and IEC61131 PLC. Examples are multi-language support and 3rd-party Robot integration(e.g. ABB)

  - Lead theh team to develop the next generation of industrial HMI based on Microsoft .NET 2.0 Framework using C# and C++/CLI.

  - Investigating for a new cost effective controls system(Hardware and Software)


Company Profile: 
Husky Injection Molding Systems Ltd. is the world's largest brand name supplier of injection molding equipment and services to the plastics industry.

### STMicroelectronics Inc.

Electronics·Micro-electronics| Foreign Company / Rep. Office| 100 - 499

Position: Leader of Embedded System Solutions |MPG Competence Center(2003/09 - Present)
Location: Shanghai
Reporting to: Manager of MMCC, manager of Division(France)
Number of People under My Supervision: 4 People
Responsibilities & Achievements: 
Firstly Team Leader of  Serial Non-Volatile Memory Support Team, then Team Leader of Embedded System Solutions Team.  Acting as an extended technical strength of Central R&D in A/P,  and on behalf the Serial Non-Volatile Memory Division, lead and manage a competent team to take the resonsibility of supporting all application activities in the whole A/P region: 

-  Embedded application reference design(software code writing and verification hardware design inclusive) based on MCUs, DSPs or ARM7/9 CPUs, focusing on memory products and their function illustrations in application fields.
-  Cross-Platform, re-usable driver code writing.
-  Memory chips function verification, datasheet drafting. 
-  PC based E-tools programming. 
-  Center Project Documents management. 

Achievements include:
Leading in a uPSD(Intel 8032 core inside) based BIOS Memory reference design; 
Leading in a uPSD(Intel 8032 core inside) based Serial Flash reference design;
Leading in an ST7(Motorola core inside) based Serial Flash Programmer design; 
Development of a project tracking system(Approx. 50,000 lines of code), based on XML and ADO to synchronize projects between Shanghai, Italy and France; 
Participance in the ARM7/9 based generic programmer for NAND/NOR Flashes,
Porting uC/OS to STR710.
Porting uCLinux to STR710.


### Schneider Electric Investment Co

Foreign Company / Rep. Office| 50 - 99

Position: Design Engineer, Project Leader |China Design Center(2002/04 - 2003/08)
Location: Shanghai
Reporting to: Manager of CDC
Number of People under My Supervision: 3 People
Responsibilities & Achievements: 
-  Electrical/electronic design of new products. 
-  Qualification test plan draft for new designed products according to IEC/GB standards. 
-  Preliminary qualification test on new product samples according to IEC/GB standards. 
-  Localization design. 
-  Technical training for technician in the lab. 
-  Development h/w and s/w tools for lab use. 
-  Advanced technical support for local Joint Ventures. 

Led a team to develop a new generation of auto-transfer system with more intelligence, friendlier HMI, higher reliability and lower cost.This NEZA PLC-based auto-transfer system that, by working with NS circuit breakers, provides reliable continual power supply where power faults may lead to critical results (e.g. hospital). led and pushed this project from pure idea to the last phase of feasibility by designing and building up 3 consecutive samples. Completed work includes all DVC analysis, technical feasibility and qualification plan. This new design saves 1/3 less cost. 
Developed a software for the lab to manage homologation tests using VC++ and Access.


### Harbin Inst of Tech. Electrical Apparatus Research Institute
Education·Training·Scientific Research·Academy Institute| Public Utility| 1 - 49

Position: R&D Staff, post-graduate student(1999/04 - 2002/01)
Location: Harbin
Reporting to: Head of Institute
Number of People under My Supervision: 2 People
Responsibilities & Achievements: 
-  Control & automation equipments development. 
-  Research on real-time control & measurement methodology and reliability. 
-  Research industrial control and automation system, including Direct Control System, Discrete Control System (DCS), Fieldbus Control system (FCS). 
-  Research on MCU system, real-time embedded system (RTOS) and embedded application development. 
-  Research on electrical/electronic interface and bus protocols. 

At the request of Heilongjiang Railway Bureau, developed 2 automation systems: Electronic Ballast Automatic Mantainence Platform and Low Voltage Apparatus Automatic Test&Maintenance Platform. 
These projects were an immediate success both academically and commercially, with the latter one being creditted by Heilongjiang Province Science & Technology committee as "Leading across the country" and being introduced across the province. 2 paper was published accordingly, one on a national academic periodical and another on an international meeting.

Reason for Quit: graduation
Project Management Experience2006/06 - Present
Project Name: HMI Re-Architecture
Software Environment: .Net 2.0, C#, C++
Hardware Environment: PC
Developing Tools: Visual Studio 2005
Project Description: 
For the following facts: 1) The current HMI has been used for many years and has evovled to be too expensive to maintain ；2) As the 1st generation of PC-based control system since PLC, it has been suffering from inherent  architectural  defaults. 3) Rapid progress in Software engineering methods and tools enables a cost-efficient re-architecture now.

This project covers a re-architecture of the HMI part of the control system to take advantage of .NET and C#.
My Responsibilities: 
Team Leader and Co-Project Leader.

Supervise and approve Design, ensure Test Driven Design(TDD) method to be correctly  followed. 





## Skills & Specialty

### Management
- Golbal Engineer Team startup & Management.

- Hardware Desgin

### Technical

#### Hardware

-  Electrical schematic & PCB board level design experience with Protel,experience in ISA/PCI board design.
-  CPLD/FPGA design with Verilog HDL. 
-  Simulation with Matlab etc. 
-  Experience on MCS51,ARM & DSP 

#### Software

-  6 years of C++ cross-platform experience in Industrial PC, embedded area and desktop application. Proficient use of Visual C++,Keil C, NI Labview environment. 
-  In-depth study of various Object Oriented Programming(OOP)/Generic Programming(GP) design patterns.
-  Familiar with uC/OS，eCos  and uCLinux embedded real-time systems (RTOS). 
-  XML experience.
-  .NET experience with C++/CLI and C#.
-  Database with ADO experience.
-  Networks Experience with SOCKET,  ACE(Adaptive Communication Environment) .
-  Experience with PLC,Fieldbus control system (FCS) .
-  Experience with various Serial & Parallel communication interfaces and protocols such as RS232/485, USB, SPI. 
-  Hardware drive and control programming, including Numega DriverWorks experience



Publications* 2 academic papers published on a national transaction and an international meeting during post-graduate study .

-  Zhai guofu, Tan Zhi, et al. The development of integral testing equipment for electronic ballasts and inverters used in trains.Low Voltage Apparatus
-  Zhai guofu, Tan Zhi, et al. Research on a test system of performance&reliability of rolling-stock circuit breakers&contactors. 8th International Low Volatage Apparatus Reliability Conference

-  Sharing 
-  http://www.codeproject.com/cpp/LiteralConversion.asp

A helper class to convert integer literals between decimals, octals, binaries and hexadecimals. 




Hobbies*  Reading, Music, Sports, Traval

