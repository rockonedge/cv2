---
- at: st
  category: projects
  duration:
    end: '2004.03'
    start: '2003.12'
  keywords:
  - 嵌入式软件设计
  - 电子电路设计
  language: zh
  name: BIOS-Flash-闪存芯片功能验证及演示平台
  summary: ''
  title: BIOS Flash 闪存芯片功能验证及演示平台
- at: st
  category: projects
  duration:
    end: '2004.10'
    start: '2004.05'
  keywords:
  - 嵌入式软件设计
  - 电子电路设计
  language: zh
  name: 嵌入式系统-SPI-FLASH-闪存芯片应用包
  summary: ''
  title: 嵌入式系统 SPI FLASH 闪存芯片应用包
- at: st
  category: projects
  duration:
    end: '2005.11'
    start: '2003.09'
  keywords:
  - 嵌入式软件设计
  - 桌面软件设计
  - 电子电路设计
  - 应用架构
  language: zh
  name: SPI-Flash-闪存芯片编程器
  summary: ''
  title: SPI Flash 闪存芯片编程器
- at: st
  category: projects
  duration:
    end: '2005.09'
    start: '2005.04'
  keywords:
  - 嵌入式软件设计
  - 电子电路设计
  language: zh
  name: 嵌入式实时操作系统-uCLinux-移植
  summary: ''
  title: 嵌入式实时操作系统　uCLinux　移植
- at: st
  category: projects
  duration:
    end: '2005.09'
    start: '2005.04'
  keywords:
  - 嵌入式软件设计
  - 电子电路设计
  language: zh
  name: 嵌入式实时操作系统-uCOS-II-移植
  summary: ''
  title: 嵌入式实时操作系统　uCOS/II　移植
- at: st
  category: projects
  duration:
    end: '2004.06'
    start: '2004.02'
  keywords:
  - 桌面软件设计
  language: zh
  name: Anatidae-项目管理系统
  summary: ''
  title: Anatidae　项目管理系统
---
# 意法半导体 

## PC BIOS Flash 闪存芯片功能验证及演示平台
**2003.12- 2004.03**
**嵌入式软件设计, 电子电路设计**
 
为新推出的 M50 系列 PC BIOS 专用 Firmware Hub/Low Pin Count Flash 总线存储芯片, 提供功能验证平台, 及用户开发演示代码。包括: 

- 功能验证和代码演示的硬件平台; 
- 分成 2 层的驱动代码（C语言）. 
    - 顶层（协议层）: 提供 Firmware Hub/Low Pin Count 协议的封装, 以标准 C 编写, 独立于硬件设计. 
    - 底层（物理层）: 提供硬件平台相关的地址, 数据, 控制相关基本操作代码, 与框架.
 
与芯片设计工程师合作, 全部负责软、硬件设计

## SPI FLASH 闪存芯片嵌入式系统应用包
**2004.05 - 2004.10**
**嵌入式软件设计, 电子电路设计**

该应用包源于日常工作需求

- 验证 IC 样片
- 演示应用场景
- 提供免费可下载 C 语言通用驱动代码库, 用于读写寄存器, 传输数据

本人负责方案设计以及全部硬件与软件设计

- 采用 [Protel/Altium][#ALTIUM] 设计了采用 uPSD3300 MCU 的嵌入式应用开发板硬件
    - 该应用也因此同时作为为即将推出的SoC(System On Chip)产品 uPSD3300提供开发包,
- 编写调试了通过 SPI 接口访问 M25/M45P(E) 系列程序/数据存储芯片的通用驱动库
- 编写了典型应用
    - 演示字符集存储, 可用于公共场所公告牌显示
    - 音频存储等应用, 可用于 MP3 播放器设计。 
- 编写英文应用指南( Application Notes ), 详细解释全部设计细节

全部软代码, 及应用指南( Application Notes )发布在 ST 官网上共供用户免费下载。

[#ALTIUM]: http://www.altium.com/

## SPI Flash 闪存芯片编程器
**2003.09 - 2005.11**
**嵌入式软件设计, 桌面软件设计, 电子电路设计, 应用架构**

基于以前原有 VB6 的半成品原型，在理解需求和现有技术架构后， 完全重构重新设计。 包括

- 上位机 PC 端界面
    - GUI 图形界面，便于直观操作。
    - CLI 命令行界面，便于批处理与第 3 方集成
- USB 通讯
    - Windows 端 USB 驱动程序
    - MCU 端 USB 驱动程序
- SPI/I&sup2;C 通讯协议及 Flash 芯片命令的组织和解析

该套件 PC 端以 C++/Python 编写, MCU 端采用 ST7 以 C 及汇编语言编写。
该套件被 ST 时期的法国同事商业化，成为其初创公司 [Dediprog][#DP] 主打产品 SFxx系列。在其创业初期，以代理 CTO 身份帮助维护开发维护产品。 **在后来的闲聊中，得知  [Dediprog][#DP] 后续维护开发的工程师对其软件架构高度称赞**， 从 2005 年最初版本至今 10 多年来，支持的芯片种类大幅增加 ， 该架构一直维持无大改动。


## 嵌入式实时操作系统　uCLinux　移植
**2005.04 - 2005.09**
**嵌入式软件设计, 电子电路设计**

该项目用于展示如何从 M25Pxx code Flash芯片启动操作系统(Embedded OS), 采用了基于 ARM 内核的 ST MCU 处理器。

采用 C 语言及汇编语言（Assembly）。

## 嵌入式实时操作系统　uCOS/II　移植
**2005.04 - 2005.09**
**嵌入式软件设计, 电子电路设计**

该项目用于展示如何从 M25Pxx code Flash芯片启动操作系统(Embedded OS), 采用了基于 ARM 内核的 ST MCU 处理器。

采用 C 语言及汇编语言（Assembly）。

## Anatidae　项目管理系统
**2004.02 - 2004.06**
**软件架构, 软件设计**
 
日常项目跟踪软件, 用以同步上海　MMCC　内部, 以及与法国 Serial Flash Division/意大利 NAND Flash Division 之间的项目进度。
采用C++语言编写, 用到　MFC, STL, BOOST。采用 ADO + Access 构建本地数据库, 以 XML 同步异地数据。
 
负责软件架构及代码编写。