---
- at: st
  category: projects
  duration:
    end: '2005.09'
    start: '2005.04'
  keywords: []
  language: en
  name: Porting-Embedded-Operating-Systems-
  summary: ''
  title: 'Porting Embedded Operating Systems, '
- at: st
  category: projects
  duration:
    end: '2004.10'
    start: '2003.12'
  keywords: []
  language: en
  name: Driver-and-Application-Demos-for-Flash-Memories
  summary: ''
  title: Driver and Application Demos for Flash Memories
- at: st
  category: projects
  duration:
    end: '2004.06'
    start: '2004.02'
  keywords: []
  language: en
  name: Memory-Competence-Center-MMCC-Project-Tracking-System
  summary: ''
  title: Memory Competence Center(MMCC) Project Tracking System
- at: st
  category: projects
  duration:
    end: '2003.12'
    start: '2003.09'
  keywords: []
  language: en
  name: Serial-Flash-Programmer-Tool-Kit
  summary: ''
  title: Serial Flash Programmer Tool Kit
---
# STMicroelectronics

## Porting Embedded Operating Systems, 
**2005.04 - 2005.09**

Based on ARM7 ST7 CPU, meant to showcase the usages, read/write performance specifically, of ST's M45/M25 SPI flashes in embedded apparatuses.

Ported uCLinux and uc/OS-II.

Wrote code in Assembly and C with RealView Development Suite/Keil.

## Driver and Application Demos for Flash Memories
**2003.12 - 2004.10**

Designed an embedded system for demo & verification of SPI interfaced Flash/EEPROM chips.

- used ST's own MCU chip
- designed PCBs with Altium
- wrote code in C language
  - portable and reusable driver code in C language
    - published on ST's website for download
    - SPI flash & EEPROM chips, e.g. M25Pxx code flash memories/ M45PE data flash memories
    - Firmware Hub(FWH) and Low Pin Count(LPC) chips, e.g M50xx flash memory 

  - showcased driver code with an application demos rotating quotes from _Albert Camus_ on a LCD display, of which my favorite one was

  > Don't walk in front of me;
  > I may not follow.
  > Don't walk behind me;
  > I may not lead.
  > Just walk beside me and be my friend.

- authored&published corresponding application notes, in English, explaining the rationale and usage of above application


## Memory Competence Center(MMCC) Project Tracking System
**2004.02 - 2004.06**

A Project tracking tool to synchronize projects

- between MMCCs at Shanghai and Prague, Czech Republic.
- between MMCC Shanghai and Divisions in France/Italy.

- written in C++, MFC/STL/BOOST.
- ADO + Access as for local database
- XML import/export remote synchronization


## Serial Flash Programmer Tool Kit
**2003.09 - 2003.12**

In line with Company’s effort to provide more accessible e-tools for the mass market, this kit was to facilitate the  usage of ST memory products, supporting SPI, I2C Initially. Specifically,

- testing chips with all supported page/sector read, program and erase commands
- free in-production usage of chip programming in small volumes(e.g. R&D designers at Intel Israel)

It consists of two parts:

- PC-based GUI software
  - Windows 9x, 2K, XP
- a PCB board running a ST7 based embedded system
  - connection to PC through the parallel port (LTP) or USB.

I took over from and re-designed the whole system reference a legacy half-done tool written in VB6, including

- a full functional GUI in MFC/C++
- firmware code on the ST7 board
  - enabled the chip-specific USB buffering techniques
    - coded against the Data Transfer Co-processor(DTC), a 2nd processor handling USB data transfer in parallel to the main CPU
      - with its specific Assembly instruction
      - within 128-byte code space

Being the best tool in class, it was later licensed by my then-colleagues from ST to have started up a company named [Dediprog Inc.]( http://www.dediprog.com/), branded as SF100/SF200/SF300 series. I was the interim CTO during the startup for a short time until I found a replacement and stayed technically involved for a few years afterwards. 
According to [this manual](http://www.dediprog.com/save/84.pdf/to/dp_SF%20User%20Manual_EN_V6.7.pdf), this features stays largely the same as what I left.  
