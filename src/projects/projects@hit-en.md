---
- at: hit
  category: projects
  duration:
    end: '2001.04'
    start: '2000.09'
  keywords: []
  language: en
  name: Low-voltage-Apparatus-Automatic-Maintenance-Platform
  summary: ''
  title: Low voltage Apparatus Automatic Maintenance Platform
- at: hit
  category: projects
  duration:
    end: '1999.10'
    start: '1999.04'
  keywords: []
  language: en
  name: Electronic-Ballast-Test-Platform
  summary: ''
  title: Electronic Ballast Test Platform
---
# Harbin Institute of Technology

## Automatic Test Platform for Rolling Stock Apparatus
**2000.09 - 2001.04**


An Industrial PC-based (IPC) automation system bid for Harbin Railway Bureau, a regional bureau affiliated to the Ministry of Railways (China).

which I took full charge of and design from scratch, including hardware, software and some mechanical design. 

The objective of the project was to provide an automatic and all-in-one platform to cover the routine maintenance check of all low voltage apparatuses used on the train: 

- circuit breakers
- contactors, and
- thermal relays

The checklist included

- electrical performance, e.g. 
  - contact resistance
  - make/break time
  - over-voltage
  - under-voltage
  - over-current 
  - short circuit etc. 
- mechanical performance 
  - sinusoidal vibration.

- Hardware design 

  - ISA board for digital I/Os
  - PCI board for data sampling and A/D conversion
  - Timer/counter circuit design
  - digital-coded power supply ranging between 1A-1300A or 0-440VAC/220VDC
  - Precise resistant load (resolution: 1/512 ohm)
  - High current sample and switch circuit.


- Software development
  - Motor control &　Hardware driving
  - Precise voltage/current control algorithm
  - Complete and user-friendly performance parameter display
  - Failure report and Lifetime estimation(FMEA)
  - Test data management

Awards&Achievements:

- Awarded 1st Prize of technical innovations by Provincial Bureau of Education, 12/2002
- Awarded 2st Prize of technical innovations by Provincial Bureau of Science & Technology in 05/2003
- An academic paper was published on *the 8th International Low Voltage Apparatus Reliability Conference*

## Auto Test Platform for Electronic Ballast 
**1999.04 - 1999.10**

Another Industrial PC-based (IPC) system designed for Harbin Railway Bureau. 

Ojective

- up to 8 electronic ballasts (15W, 20W, 30W, 40W) concurrently, 
- detailed performance parameters such as 
  - voltage, 
  - current, 
  - power factor, 
  - harmonic distortions(THD), 
  - temperature rise etc. 
  - real-time voltage and current waveform 
  - FMEA reports
  - historic events database
  - pretty print

- hardware
  - ISA I/O board, A/D board
  - Power supply board (DC 47V-57V, AC 200V – 240V)
  - Sensor board (voltage, current and temperature)
  - Target-switching board

- software
  - Hardware driving
  - Real-time sample and analysis (FFT algorithm)
  - Rendering visual waveform
  - Test data management

This platform greatly sped up the routine check efforts of technicians who were dealing with tens of electronic ballasts daily. The usages were spreaded to another 5 railway bureaus soon after the completion for its satisfactory performance.

A paper was published on a national academic periodical explain the design.

It was all of my work from initial research, hardware design, software design spec and debug, except for the software UI design.
