---
- at: seci
  category: projects
  duration:
    end: '2003.06'
    start: '2002.11'
  keywords: []
  language: en
  name: Automatic-Power-Transfer-System
  summary: ''
  title: Automatic Power Transfer System
- at: seci
  category: projects
  duration:
    end: '2003.06'
    start: '2002.11'
  keywords: []
  language: en
  name: Development-of-industrial-strength-LED-alarms
  summary: ''
  title: Development of industrial-strength LED alarms
---
# Schneider Electric

## Automatic Power Transfer System
**2002.11 - 2003.06**

 a set of critical switching unit for dual power supplies ensuring reliably & continual power supply where power faults may lead to critical results (e.g. surgical rooms in hospitals).

 There was no out-of-box product on the market, except for ad-hoc solutions built up with components from vendors of customers choice.

The goal was to develop a complete auto-transfer system aimed for

- more intelligence
- friendlier HMI
- higher reliability
- lower cost

All objectives achieved using all Schneider products

- NEZA PLC for intelligence and user-friendliness
- popular NS circuit breakers for reliability
- safety achieved with interlocks
  - software/electrical interlock by PLC
  - built-in mechanical interlock by NS

Led a team from multiple departments (Design center, Marketing, Field Application) and pushed from concepts to fully-functional prototype, includes analysis reports on
- cost (only 1/3 of solution on the market)
- technical risks
- qualification plan

It was submitted for Chinese Management Committee(CMC) for final review of production investment by the time I left.

## Development of industrial-strength LED alarms
**2002.11 - 2003.06**

research to in preparation for launch Schneider branded alarms for factory shopfloors.
- investiagate LED technologies.
- collect LED samples from select vendors  
- devise tests for IEC conformance tests regarding lumen, product life etc.
