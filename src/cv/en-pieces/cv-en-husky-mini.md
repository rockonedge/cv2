---
language: en
2: ['Experience']
3: ['Team Leader/Founder, Global Development Engineering Department']
category: cv
---
## **Experience**

### **Team Leader/Founder, Global Development Engineering Department**
- **[Husky Injection Molding Systems][#HUSKY], 11/2005 - 12/2016**
- **Reported to Director of Global Development Engineering(HQ in Canada)**

The founder&top regional leader of a global engineering team with direct reports to Canada. Grew team from 0 to 5, and then to 12 headcounts 2 years later due to exceptional team performance.

Helped to start up from the scratch the manufacturing capacity of injection molding machines at Shanghai campus, and made [**Significant Contribution** acknowledged by the then Global Vice President of Machines](./pics/husky-contribution.jpg).

Worked with Canadian Development Engineering teams daily, through email, audio/video conference calls and mutual visits.

**Starting from 2014, focused on advocating Virtualization/DevOps/Cloud/Big Data/Mobile technologies inside the company, towards Industry 4.0/IIoT solutions, leveraging Iaas/Paas/SaaS offerings from Amazon AWS/ Microsoft Azure, e.g. AWS IoT, Azure IoT Hub etc.**

**As the founder&top regional leader of the team, ultimately responsible for Shanghai team with direct reports to the headquarter**.

Controls system development for the injection molding sector, mostly software(desktop, firmware and server/cloud) design along with electronic, electrical, mechanical tasks, including HMI, PLC and MES/SCADA in C#, C++, Python Ruby, JavaScript etc. for Windows, Linux and the cloud.

**Major Achievements**

- led the acquisition of [Moldflow][#BUYMF] from French/US teams to Shanghai
- Shipped [Shotscope NX](http://www.husky.co/EN-US/Shotscope-NX.aspx), the industry’s most advanced plant-wide process and production monitoring system
- Rolling-shipped Polaris, the primary software platform(HMI, PLC and Production System in C#, C++, Ruby etc.) for all injection machines for 11 years
- [**Significant Contribution Award**](./pics/husky-contribution.jpg), Signed by the then Global Vice President of Machines, 2007
- **Top-Ranked Team Leader**, 2011
  - Husky's global employee survey conducted by [Kenexa][#KENEXA]
- **10-Year Service Award**, 2015


[**See Feature Projects >>**](https://ttan.netlify.com/cv/?src=projects-en)