---
language: en
3: ['Electronic Design Engineer, China Design Center']
category: cv
---
### **Electronic Design Engineer, China Design Center**
- **[Schneider Electric (China) Investment Co.][#SECI], 4/2002 - 8/2003**
- **Reported to Manager of China Design Center(French Expat)**

Worked closely with the lab technicians, providing premium technical support for local joint ventures on industrial control&automation(ICA) products, including

- electrical/electronic localization design
​    - Authored and conducted homologation tests for IEC/GB standards compliance for local joined ventures products
- development of lab equipment / utilities

**Major Achievements**

- Auto-Transfer System(ATS), an emergency backup power switch for critical sectors like hospitals built with PLC and circuit breakers
