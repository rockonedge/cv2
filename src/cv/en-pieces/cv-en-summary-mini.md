---
language: en
2: ['Summary']
category: cv
---
## **Summary**

- 19+ years of R&D-focused experience of
  - software/firmware across cloud/server/desktop/embedded platforms, including Windows, Linux and Microsoft Azure
  - schematic&PCB design and simulation
  - programming with C++/Python/C#/JavaScript/Go/Matlab


- 15+ years of professional career in World's top companies with 
  - industrial automation, electronics and manufacturing
  - abundant cross-team experience supporting production, sales and service teams


- 11+ years of leading a successful global engineering team
  - multiple global engineering projects accomplished
  - leadership acknowledged from within and outside of the team

- Lead by example
- Great self-motivation
- Visionary and open minds
- enthusiastic pursuer of knowledge and challenges