---
language: en
3: ['Senior Application Engineer/Interim Team Leader, Memory Competence Center']
category: cv
---
### **Senior Application Engineer/Interim Team Leader, Memory Competence Center**
- **[STMicroelectronics][#ST], 9/2003 - 11/2005**
- **Reported to Departmental Manager(Shanghai) & R&D Manager(Division in France)**

A startup member and interim leader for the *Serial Non-Volatile Memory & Embedded System Solutions Team*, explored and built a connection with the R&D Division in France, and maintained effective communication.

Daily work included

- electronic Schematic/PCB layout,
- embedded/desktop software/firmware coding in C/C++/ASM
- English application notes authoring

**Major Achievements**
- completed driver code and reference design for full-range M50xx BIOS memory chips
- completed all driver code and reference design for M25xx/M45 SPI code/data memory chips
- initiated a startup [Dediprog Inc.][#DP] extending my work in ST with French colleagues, being the interim CTO 

### **Co-Founder/Interim CTO, A Global Start-up**
- **[Dediprog Inc.][#DP], 6/2005 - 6/2009**

Together with colleagues from ST France and Shanghai, and with license from ST, started up a company dedicated to flash memory programming&verification toolkits.

In charge of all things technology, esp. software both on PC and MCU. 

- Built a full-blown memory chip programmer
  - based on the application kit(H/W & F/W) I developed in ST
  - implemented consumer-focused features, e.g.
      - non-ST chips, e.g. Atmel, SST
      - feature for R&D and mass production scenarios
  - Development
      - Desktop: Re-Architecture
          - Overhauled GUI, USB driver and database on PC with C++
      - Embedded: Enhanced F/W ST7 (Assembly for a DTC co-processor)
- Serving key customers, e.g Intel's R&D Engineers at Israel

After the successful startup and healthy running, transfered my role from a partner to consultant and remained so till 2009. 

  