#
# this script create a out-of-source build folder
#
import sys

from datetime import date, datetime
import os
from os import listdir, path
import shutil
import concurrent.futures
from pathlib import Path

import logging
import enable_logging
enable_logging.init()


logger = logging.getLogger('cv')

info = logger.info
debug = logger.debug


def shell_run(command):
    import subprocess

    try:
        cp = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        logger.info(f'args={cp.args}, returncode={cp.returncode}')
        logger.debug(f'stdout={cp.stdout}, stderr={cp.stderr}')
    except:
        pass

def this_folder():

    return Path(os.path.abspath(__file__)).parent

def copy_fonts(frm):
            # copy fonts
        # https://askubuntu.com/questions/3697/how-do-i-install-fonts
        # https://wiki.ubuntu.com.cn/%E5%AD%97%E4%BD%93

        if os.name == 'posix':
            Copier.tree_copy(frm, Path.home() / '.fonts')
            shell_run(['fc-cache', '-fv'])

def copy_landing_page(frm, to):
    frm = Roots.script_root / 'sandbox' / 'landing-page' / 'vertical-timeline'
    css = to / 'css'
    js = to / 'js'
    img = to / 'img'

    for d in [css, js, img]:
        d.mkdir(exist_ok=True)

    Copier.copyfile(frm / 'index.html', to / 'index.html')
    Copier.copyfile(frm / 'css' / 'landing.css', css / 'landing.css')
    Copier.copyfile(frm / 'js' / 'landing.js', js / 'landing.js')
    Copier.copyfile(frm / 'js' / 'modernizr.js', js / 'modernizr.js')
    Copier.copyfile(frm / 'js' / 'jquery.min.js', js / 'jquery.min.js')
    Copier.copyfile(frm / 'img' / 'cd-icon-location.svg', img / 'cd-icon-location.svg')

def copy_plain():
    # copy template:  json setting file
    # for sub in ['js', 'css', 'bootswatch-themes']:
    Copier.tree_copy(Roots.mdcv/'template', Roots.build_plain_output)
    # copy markdown-cv pythong scripts and html templates
    Copier.tree_copy(Roots.mdcv, Roots.build_plain)

    # copy src:  json setting file
    Copier.copyfile(Roots.src / 'markcv.json',
                    Roots.build_plain / 'markcv.json')
    Copier.flat_copy(Roots.src, Roots.build_plain / 'markdown', ['.md'])
    Copier.tree_copy(Roots.src / 'pics', Roots.build_plain_output/'pics')
    Copier.tree_copy(Roots.src / 'pics', Roots.build_plain_output/'md'/'pics')

def copy_vue_dist(frm, to, name):

    info(f'building {name}... ')
    root = Path(__file__).absolute().parent
    shell_run(f'cd {root}/{name}/ && npm install && npm run build && cd ..')
    info(f'done building {name}.')

    info(f'copying {name}... ')

    Copier.copyfile(frm / 'index.html', to / 'index.html')

    for d in [frm, to]:
        d = frm / 'dist'
        d.mkdir(exist_ok = True)

    # temp fix for npm version bug on netlify
    Copier.tree_copy(Roots.src /'pics', Roots.vue_landing / 'dist'/ 'pics')
    

    Copier.tree_copy(frm/'dist', to/'dist')

    info(f'done copying {name}.')
    
def copy_vue_landing(frm, to):
    copy_vue_dist(frm, to, 'vue-landing')

def copy_vue_cv():    
    release_vue = Roots.release /'vue'    
    release_vue.mkdir(exist_ok = True)
    
    copy_vue_dist(Roots.vue_cv, release_vue, 'vue-cv')
    # temp fix for npm version bug on netlify
    Copier.tree_copy(Roots.src /'pics', Roots.vue_cv / 'dist'/ 'pics')
    

def copy_netlify(to):
    # release to netlify
    info(f'releasing to netlify')
    Copier.copyfile(Roots.script_root / 'netlify' /
                    '_redirects', to / '_redirects')

def make_zipfile(output_filename, source_dir):
    """ based on http://stackoverflow.com/a/17080988"""
    import zipfile

    relroot = os.path.abspath(os.path.join(source_dir))
    with zipfile.ZipFile(output_filename, "w", zipfile.ZIP_DEFLATED) as zip:
        for root, dirs, files in os.walk(source_dir):
            # add directory (needed for empty dirs)
            zip.write(root, os.path.relpath(root, relroot))
            for file in files:
                filename = os.path.join(root, file)
                if os.path.isfile(filename):  # regular files only
                    arcname = os.path.join(
                        os.path.relpath(root, relroot), file)
                    # print(arcname)
                    zip.write(filename, arcname)

class Copier():
    @staticmethod
    def tree_copy(frm, to):
        from distutils.dir_util import copy_tree
        # Path(to).mkdir(exist_ok=True)
        output = copy_tree(str(frm), str(to))
        debug(f'files copied: {output}')

    @staticmethod
    def copyfile(frm, to):
        shutil.copyfile(str(frm), str(to))
        debug(f'{frm}  -> {to}')

    @staticmethod
    def flat_copy(src, dest, exts):
        """globs files from [src] and copy to [dest],  according to extentions"""

        dest.mkdir(exist_ok = True)

        for ext in exts:
            for img in (src).glob('**/*' + ext):
                Copier.copyfile(img,  dest / img.name)

class Roots():

    script_root = this_folder()

    db = script_root / 'db'
    vue_cv = script_root / 'vue-cv'
    mdcv = script_root / 'markdown-cv'
    src = script_root / 'src'
    build = script_root / 'temp'
    build_plain = build / 'plain'
    build_plain_output = build_plain / 'release'
    build_vue = build / 'vue'

    release = script_root / 'release'
    release_cv = release /'cv'

    vue_landing = script_root / 'vue-landing'


Roots.build.mkdir(exist_ok=True)
Roots.release.mkdir(exist_ok=True)
Roots.build_plain.mkdir(exist_ok=True)
Roots.release_cv.mkdir(exist_ok=True)


class Builder():

    @staticmethod
    def zip():

        # zip for http://ttan.getforge.io
        # info('zipping files')
        pass

    @staticmethod
    def pre_buid():
        copy_plain()
    @staticmethod
    def post_build():

        Copier.tree_copy(str(Roots.build_plain_output), str(Roots.release_cv))              

        copy_netlify(Roots.release)

        ##
        Builder.zip()

    @staticmethod
    def build(generat_merged_html, generate_pdf):

        sys.path.append(str(Roots.build_plain))
        sys.path.append(str(Roots.db / 'script'))

        import md_processor
        import singlize_html
        import html_to_pdf

        md_processor.go()

        with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:
            if generat_merged_html:
                executor.submit(singlize_html.go)
            if generate_pdf:
                executor.submit(html_to_pdf.go)

                
        import transform_cv
        import en_transform_cv
        import transform_projects_md

        transform_cv.main()
        en_transform_cv.main()
        transform_projects_md.main()


def run(generat_merged_html, generate_pdf):
    copy_fonts(Roots.mdcv/'fonts')
    
    Builder.pre_buid()
    Builder.build(generat_merged_html, generate_pdf)
    Builder.post_build()


if __name__ == "__main__":

    try:
        info(os.uname())
        info(sys.version)
        shell_run('which wkhtmltopdf')
        # shell_run(['/usr/bin/wkhtmltopdf', '-V'])
        shell_run('wkhtmltopdf -V')
        shell_run('which npm')
        shell_run('npm -v')
        shell_run('npm install npm@5.6.0 -g')        
        shell_run('which npm')
        shell_run('npm -v')
        shell_run('pandoc -v')
    except:
        pass

    generat_merged_html, generate_pdf = False, False

    argv = sys.argv
    argc = len(sys.argv)

    info(f'{argv}')

    if '--with-vue-landing' in argv:
        copy_vue_landing(Roots.vue_landing , Roots.release)
    else:
        copy_landing_page(
            Roots.script_root / 'sandbox' / 'landing-page' / 'vertical-timeline',
            Roots.release)

    
    if '--with-vue-cv' in argv:
        copy_vue_cv()

    generate_pdf = True if '--with-pdf' in argv else False
    info(f'enable generating pdf: {generate_pdf}')

    generat_merged_html = True if '--with-embedding' in argv else False
    info(f'enable merging static resource into html: {generat_merged_html}')

    start = datetime.now()
    run(generat_merged_html, generate_pdf)
    end = datetime.now()

    print(end-start)
