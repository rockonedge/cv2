

  export function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }


  // export function apply_theme() {
  //   var available_themes = {
  //     0: { 'name': 'cerulean', 'color': 'navbar-light bg-light' },
  //     1: { 'name': 'cerulean', 'color': 'navbar-dark bg-primary' },
  //     2: { 'name': 'cerulean', 'color': 'navbar-dark bg-dark' },
  //     3: { 'name': 'cosmo', 'color': 'navbar-light bg-light' },
  //     4: { 'name': 'cosmo', 'color': 'navbar-dark bg-primary' },
  //     5: { 'name': 'cosmo', 'color': 'navbar-dark bg-dark' },
  //     6: { 'name': 'custom', 'color': 'navbar-light bg-light' },
  //     7: { 'name': 'custom', 'color': 'navbar-dark bg-primary' },
  //     8: { 'name': 'custom', 'color': 'navbar-dark bg-dark' },
  //     9: { 'name': 'cyborg', 'color': 'navbar-light bg-light' },
  //     10: { 'name': 'cyborg', 'color': 'navbar-dark bg-primary' },
  //     11: { 'name': 'cyborg', 'color': 'navbar-dark bg-dark' },
  //     12: { 'name': 'darkly', 'color': 'navbar-light bg-light' },
  //     13: { 'name': 'darkly', 'color': 'navbar-dark bg-primary' },
  //     14: { 'name': 'darkly', 'color': 'navbar-dark bg-dark' },
  //     15: { 'name': 'flatly', 'color': 'navbar-light bg-light' },
  //     16: { 'name': 'flatly', 'color': 'navbar-dark bg-primary' },
  //     17: { 'name': 'flatly', 'color': 'navbar-dark bg-dark' },
  //     18: { 'name': 'journal', 'color': 'navbar-light bg-light' },
  //     19: { 'name': 'journal', 'color': 'navbar-dark bg-primary' },
  //     20: { 'name': 'journal', 'color': 'navbar-dark bg-dark' },
  //     21: { 'name': 'litera', 'color': 'navbar-light bg-light' },
  //     22: { 'name': 'litera', 'color': 'navbar-dark bg-primary' },
  //     23: { 'name': 'litera', 'color': 'navbar-dark bg-dark' },
  //     24: { 'name': 'lumen', 'color': 'navbar-light bg-light' },
  //     25: { 'name': 'lumen', 'color': 'navbar-dark bg-primary' },
  //     26: { 'name': 'lumen', 'color': 'navbar-dark bg-dark' },
  //     27: { 'name': 'lux', 'color': 'navbar-light bg-light' },
  //     28: { 'name': 'lux', 'color': 'navbar-dark bg-primary' },
  //     29: { 'name': 'lux', 'color': 'navbar-dark bg-dark' },
  //     30: { 'name': 'materia', 'color': 'navbar-light bg-light' },
  //     31: { 'name': 'materia', 'color': 'navbar-dark bg-primary' },
  //     32: { 'name': 'materia', 'color': 'navbar-dark bg-dark' },
  //     33: { 'name': 'minty', 'color': 'navbar-light bg-light' },
  //     34: { 'name': 'minty', 'color': 'navbar-dark bg-primary' },
  //     35: { 'name': 'minty', 'color': 'navbar-dark bg-dark' },
  //     36: { 'name': 'pulse', 'color': 'navbar-light bg-light' },
  //     37: { 'name': 'pulse', 'color': 'navbar-dark bg-primary' },
  //     38: { 'name': 'pulse', 'color': 'navbar-dark bg-dark' },
  //     39: { 'name': 'sandstone', 'color': 'navbar-light bg-light' },
  //     40: { 'name': 'sandstone', 'color': 'navbar-dark bg-primary' },
  //     41: { 'name': 'sandstone', 'color': 'navbar-dark bg-dark' },
  //     42: { 'name': 'simplex', 'color': 'navbar-light bg-light' },
  //     43: { 'name': 'simplex', 'color': 'navbar-dark bg-primary' },
  //     44: { 'name': 'simplex', 'color': 'navbar-dark bg-dark' },
  //     45: { 'name': 'sketchy', 'color': 'navbar-light bg-light' },
  //     46: { 'name': 'sketchy', 'color': 'navbar-dark bg-primary' },
  //     47: { 'name': 'sketchy', 'color': 'navbar-dark bg-dark' },
  //     48: { 'name': 'slate', 'color': 'navbar-light bg-light' },
  //     49: { 'name': 'slate', 'color': 'navbar-dark bg-primary' },
  //     50: { 'name': 'slate', 'color': 'navbar-dark bg-dark' },
  //     51: { 'name': 'solar', 'color': 'navbar-light bg-light' },
  //     52: { 'name': 'solar', 'color': 'navbar-dark bg-primary' },
  //     53: { 'name': 'solar', 'color': 'navbar-dark bg-dark' },
  //     54: { 'name': 'spacelab', 'color': 'navbar-light bg-light' },
  //     55: { 'name': 'spacelab', 'color': 'navbar-dark bg-primary' },
  //     56: { 'name': 'spacelab', 'color': 'navbar-dark bg-dark' },
  //     57: { 'name': 'superhero', 'color': 'navbar-light bg-light' },
  //     58: { 'name': 'superhero', 'color': 'navbar-dark bg-primary' },
  //     59: { 'name': 'superhero', 'color': 'navbar-dark bg-dark' },
  //     60: { 'name': 'united', 'color': 'navbar-light bg-light' },
  //     61: { 'name': 'united', 'color': 'navbar-dark bg-primary' },
  //     62: { 'name': 'united', 'color': 'navbar-dark bg-dark' },
  //     63: { 'name': 'yeti', 'color': 'navbar-light bg-light' },
  //     64: { 'name': 'yeti', 'color': 'navbar-dark bg-primary' },
  //     65: { 'name': 'yeti', 'color': 'navbar-dark bg-dark' }
  //   }

  //   var loc = '';
  //   if (getParameterByName('loc', location.search)) loc = 'zh-';


  //   var theme = getParameterByName('theme', location.search);

  //   theme = available_themes[theme] || available_themes[40];
  //   console.info('theme = ' + JSON.stringify(theme))




    
  // }
