
export function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

export function apply_theme(css_location) {

  let stock_themes = ['cerulean',
    'cosmo',
    'cyborg',
    'darkly',
    'default',
    'flatly',
    'journal',
    'litera',
    'lumen',
    'lux',
    'materia',
    'minty',
    'nuclear',
    'pulse',
    'sandstone',
    'simplex',
    'slate',
    'solar',
    'spacelab',
    'superhero',
    'united',
    'yeti'];

  let default_theme = "darkly";

  // let loc = "zh-";
  // if (getParameterByName("loc", location.search)) loc = "";
  let theme = getParameterByName("theme", location.search);
  if (stock_themes.indexOf(theme) > 0) default_theme = theme;


  let css = css_location + '/zh-' + default_theme + ".css";

  let element = document.getElementById("theme_css")
  if (element) {
    element.href = css;
  } else {
    document.querySelector('head').innerHTML += '<link rel="stylesheet" href="' + css + '" type="text/css"/>';
  }

  console.log(css);
}