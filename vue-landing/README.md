# This folder is generated with 
```bash
vue init webpack-simple webpack-landing
```




# webpack-landing

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).



## tips:
to include jquery and popper as global
- in main.js

```
import './assets/js/jquery.min.js'
import './assets/js/popper.min.js'
import './assets/js/bootstrap.min.js'
```

- in webpack.config.js
```

  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      'jquery': path.resolve(__dirname, './src/assets/js/jquery.min.js'),
      'popper': path.resolve(__dirname, './src/assets/js/popper.min.js'),
    },
    extensions: ['*', '.js', '.vue', '.json']
  }, 
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jquery: 'jquery',
      'window.jQuery': 'jquery',
      jQuery: 'jquery',
      Popper:'popper'
    })
  ],
  ```
