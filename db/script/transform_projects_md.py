from os import path, listdir
from pprint import pprint
from datetime import datetime, date
from pathlib import Path
import yaml

import re
from itertools import groupby

from roots import Roots

from toc import TOC, TocFormatter

def yaml_dump(header_block):                    
    return f'---\n{yaml.dump(header_block, default_flow_style=False, allow_unicode=True)}---\n'

class ProjectMdFile():
    ''' 1. split file into yaml header and md
        2. split md into individual projects, called section
        3. parse each project with the following canonical format
            ## tile, 
            ** duration
            ** keywords
            <blank line> 
            body 
    '''
    def __init__(self,  filename, extra_header = {}):
        self.filename = filename
        self.extra_header = dict(extra_header, **self._meta_from_name())
        self._content = Path(filename).read_text(encoding='utf-8')

        self._run()


    def _meta_from_name(self):
        language = 'en' if 'en' in str(self.filename) else 'zh'
        # at = self.filename.stem.split('@')[1].split('-')[0]

        return {'language': language}
        # return {'language': language, 'at': at}
       
    def _split_file(self):

        import re

        yaml_md = re.split(r'^---$', self._content, 2, re.MULTILINE)

        yml = yaml.load(yaml_md[-2] if len(yaml_md)  == 3 else '')
        md = yaml_md[-1].lstrip()

        if md.startswith('# '):
            at, md = md.split('\n', 1)
            self.extra_header['at'] = at.replace("# ", "")

        return yml, md


    def _section_to_dict(self, s):

        for i, line in enumerate(s):
            if not line.strip():
                header = s[0:i]
                body = s[i+1:]
                break
        # title
        title = header[0][3:].strip()

        # duration
        start, end = '', ''
        if len(header) > 1:
            duration =  re.sub(r'[^\d.-]','', s[1]).split('-')
            start = duration[0].strip()
            end = duration[1].strip() if len(duration) > 1 else start
        
        # keywords
        keywords = []
        if len(header) > 2:
            keywords = [w.strip() for w in header[2].replace('*', '').split(',')]

        # one-liner summary
        one_liner = header[3].strip() if len(header) > 3 else ''

        header = dict({
                'name':re.sub(r'\W+', '-', title),
                'summary': one_liner,
                'title': title,
                'keywords': keywords,
                'duration': {
                    'start': start,
                    'end': end
                    }}, **self.extra_header)

        return dict(header=header, body='\n'.join(body))

    def _md_to_sections(self, content):
        '''split md file into sections'''

        start = 0
        sections = []
        lines = content.split('\n')
        for i, line in enumerate(lines):
            if line.lstrip().startswith('## '):
                sections.append(lines[start:i])
                start = i
        sections.append(lines[start:i])

        return [self._section_to_dict(s) for s in sections[1:]]   

    def _run(self):

        hdr, bdy = self._split_file()
        self._yml = self._md_to_sections(bdy)

    @property 
    def as_yaml(self):
        return self._yml

    def format(self, fmt):
        MdFileWriter.from_sections(self._yml, Roots.BY_FILTER_FOLDER / self.filename.name, fmt, True)

class SectionFormatter():

    @staticmethod
    def css_print(section, with_yaml_header, template):
        """ 
        - merged duration and keywords

        """

        hdr = section['header'] 
        body = section['body']

        title = hdr['title']

        duration = hdr['duration']
        start, end = duration['start'], duration['end']
        duration = ' - '.join([start, end]) if start != end else start

        keywords = ', '.join(hdr['keywords'])

        subtitle = ' &middot; '.join([duration, keywords]) if keywords else duration

        one_liner = hdr['summary']

        # return '\n'.join([f'{yaml_dump(hdr) if with_yaml_header else ""}',
        #                 f'## {title}',
        #                 f'__{subtitle}__ ',
        #                 f'\n__{one_liner}__ ' if one_liner else '',
        #                 f'\n',
        #                 f'
        #                 '])

        import markdown

        if template == 'bs4':
            return f'''<div class="card border-primary mb-3" style="max-width: 200rem;">
<div class="card-header">{subtitle}</div>
<div class="card-body">
    <h4 class="card-title">{title}</h4>
    <h6 class="card-subtitle mb-2 font-weight-bold font-italic">{one_liner if one_liner else ''}</h6>
    <div class="card-text">{markdown.markdown(body)}</div>    
</div>
</div>
'''
        if template == 'bulma':
            return f'''<article class="message">
  <div class="message-header">
    <p>{subtitle}</p>
  </div>
  <div class="message-body">
    <h4 class="title">{title}</h4>
    <h6 class="subtitle">{one_liner if one_liner else ''}</h6>
    <div class="content">{markdown.markdown(body)}</div>    
 </div>
</article>
'''

    @staticmethod
    def bs4_print(section, with_yaml_header):
        return SectionFormatter.css_print(section, with_yaml_header, 'bs4')


    @staticmethod
    def bulma_print(section, with_yaml_header):
        return SectionFormatter.css_print(section, with_yaml_header, 'bulma')

    @staticmethod
    def pretty_print(section, with_yaml_header):
        """ 
        - merged duration and keywords

        """

        hdr = section['header'] 
        body = section['body']

        title = hdr['title']

        duration = hdr['duration']
        start, end = duration['start'], duration['end']
        duration = ' - '.join([start, end]) if start != end else start

        keywords = ', '.join(hdr['keywords'])

        subtitle = ' &middot; '.join([duration, keywords]) if keywords else duration

        one_liner = hdr['summary']

        return '\n'.join([f'{yaml_dump(hdr) if with_yaml_header else ""}',
                        f'## {title}',
                        f'__{subtitle}__ ',
                        f'\n__{one_liner}__ ' if one_liner else '',
                        f'\n',
                        f'{body}'])
    
    @staticmethod
    def canonical(section, with_yaml_header):
        """ source form

            ## tile, 
            ** duration
            ** keywords
            <blank line> 
            body 
        """

        hdr = section['header'] 
        body = section['body']

        title = hdr['title']

        duration = hdr['duration']
        start, end = duration['start'], duration['end']
        duration = ' - '.join([start, end]) if start != end else start

        keywords = ', '.join(hdr['keywords'])
        one_liner = hdr['summary']

        return '\n'.join([f'{yaml_dump(hdr) if with_yaml_header else ""}',
                        f'## {title}',
                        f'__{duration}__ ',
                        f'__{keywords}__ ' if keywords else '',
                        f'\n__{one_liner}__ ' if one_liner else '',
                        f'\n',
                        f'{body}'])


def collect(root, extra_header):
    all_sections = []
    for md in root.glob('**/*.md'):    
        mp = ProjectMdFile(md, extra_header)
        mp.format(SectionFormatter.canonical)
        all_sections += mp.as_yaml 
    return all_sections

def group_sections_by(sections, fmt, key):
    sections = sorted(sections, key = key)

    lines = []
    for k, group in groupby(sections, key = key):
        print(f'key = {k}')
        lines.append(f'# {k}')
        lines.append('\n'.join([fmt(s, False) for s in group]))
    return '\n'.join(lines)


class MdFileWriter():

    @staticmethod
    def from_sections(sections, filename, fmt, with_yaml_header):

            is_css_formatted = (fmt == SectionFormatter.bs4_print or fmt == SectionFormatter.bulma_print)
            md = '\n'.join([fmt(s, False) for s in sections])
            if not is_css_formatted:
                t = TOC(md)
                md = t.with_toc

            new_header = dict(items=[s['header'] for s in sections], 
                    banner=dict(
                    title=str(filename), # http://stackoverflow.com/a/4869782
                    time = datetime.now(),
                    brand=filename.stem,
                    nav=TocFormatter.as_bs_navbar(t.toc_body[0]) if not is_css_formatted else '',
                    lastupdate= date.today()
                    ))   
            new_header = yaml_dump(new_header) if with_yaml_header else ''

            MdFileWriter.print_file(
                filename,
                new_header + md)                    

    @staticmethod
    def from_sections_groupby(sections, filename, fmt, with_yaml_header, groupby_key):

            md = group_sections_by(sections, fmt, groupby_key)
            t = TOC(md)
            md = t.with_toc


            new_header = dict(items=[s['header'] for s in sections], 
                    banner=dict(
                    title=str(filename), # http://stackoverflow.com/a/4869782
                    time = datetime.now(),
                    brand=filename.stem,
                    nav=TocFormatter.as_bs_navbar(t.toc_body[0]),
                    lastupdate= date.today()
                    ))   
            new_header = yaml_dump(new_header) if with_yaml_header else ''

            

            MdFileWriter.print_file(
                filename,
                new_header + md)

    @staticmethod            
    def from_section(section, fmt, with_yaml_header = True):
            hdr = section['header']

            MdFileWriter.print_file(
                Roots.DB_RELEASE / '.'.join([hdr['name'], hdr['language'], 'md']), 
                fmt(section, with_yaml_header))

    @staticmethod
    def print_file(filename, content):
        print(filename)
        filename.write_text( content, encoding='utf-8')




def copy_to_vue_landing(sections):
    # all as yaml
    (Roots.DB_RELEASE /'all.yml').write_text(yaml_dump(sections), encoding='utf-8')
    
    # all as json
    import json

    text = json.dumps(sections, ensure_ascii=False)
    (Roots.DB_RELEASE /'all.json').write_text(text, encoding='utf-8')
    (Roots.VUE_LANDING / 'src/assets/json/projects.json').write_text(text, encoding='utf-8')


def go(root, extra_header):

    all_sections = collect(root, extra_header)
    copy_to_vue_landing(all_sections)

    # per section 
    for s in all_sections:
        MdFileWriter.from_section(s, SectionFormatter.bs4_print)

    # per language
    key = lambda y: y['header']['at']
    for lang in ['en', 'zh']:
        sections = [s for s in all_sections if s['header']['language'] == lang]
        MdFileWriter.from_sections_groupby(
            sections, 
            Roots.BY_FILTER_FOLDER / (lang + '_group.md'),  
            SectionFormatter.canonical, 
            True,
            key)        
        MdFileWriter.from_sections_groupby(
            sections, 
            Roots.BY_FILTER_FOLDER / (lang + '-bs4_group.md'),  
            SectionFormatter.bs4_print, 
            True,
            key)

        MdFileWriter.from_sections_groupby(
            sections, 
            Roots.BY_FILTER_FOLDER / (lang + '-bulma_group.md'),  
            SectionFormatter.bulma_print, 
            True,
            key)
        print(f'{len(sections)} {lang} projects in total.')


    # all as md
        MdFileWriter.from_sections(
            all_sections, 
            Roots.BY_FILTER_FOLDER / 'all.md',  
            SectionFormatter.pretty_print, 
            True)            
        MdFileWriter.from_sections(
            all_sections, 
            Roots.BY_FILTER_FOLDER / 'bs4-all.md',  
            SectionFormatter.bs4_print, 
            True)                
        MdFileWriter.from_sections(
            all_sections, 
            Roots.BY_FILTER_FOLDER / 'bulma-all.md',  
            SectionFormatter.bulma_print, 
            True)    
    print(f'{len(all_sections)} projects in total.')

    # MdFileWriter.from_sections(
    #         sections, 
    #         Roots.BY_FILTER_FOLDER / (lang + '.md'),  
    #         SectionFormatter.canonical, 
    #         True)
    # print(f'{len(sections)} {lang} projects in total.')

    # by_at = [get_formated_body(y for y in collect(Roots.DB_RELEAS)) if y['at']]
    # to = target_md_folder() / (k + '.md')
    # to.write_text('\n\n'.join(md), encoding='utf-8')


def main():
    go(Roots.SRC_MD_FOLDER / 'projects', extra_header = {'category': 'projects'})

if __name__ == '__main__':
    main()


# def per_section(section):
#     d_edu = []
#     edu = path.join(root_folder(), section)
#     for f in os.listdir(edu):
#         p = path.join(edu, f)
#         # print(p)
#         with open(p, encoding='utf-8', mode='r') as md:
#             d_edu.append({'tag':f, 'content': ''.join(md.readlines()[2:])})

#     # print(d_edu)
#     return d_edu

# def run():

#     sections = ['education', 'employment', 'summary', 'training', 'skills', 'appendix', 'overture']
#     j = {k:per_section(k) for k in sections}
#     pprint(j)

#     import json

#     with open('out.json', encoding='utf-8', mode='w+') as out:
#         json.dump(j, out)
